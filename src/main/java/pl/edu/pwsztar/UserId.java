package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        int numberOfUserSex = id.charAt(9);
        if(numberOfUserSex%2==0){
            return Optional.of(Sex.WOMAN);
        }
        else {
            return Optional.of(Sex.MAN);
        }
    }

    @Override
    public boolean isCorrect() {
        if(!isCorrectSize()) {
            return false;
        }
        //9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
        //44051401358 is 169:10=8 false must be 9
        int sum = (9*(Integer.parseInt(String.valueOf(id.charAt(0))))) +
                (7*(Integer.parseInt(String.valueOf(id.charAt(1))))) +
                (3*(Integer.parseInt(String.valueOf(id.charAt(2))))) +
                (1*(Integer.parseInt(String.valueOf(id.charAt(3))))) +
                (9*(Integer.parseInt(String.valueOf(id.charAt(4))))) +
                (7*(Integer.parseInt(String.valueOf(id.charAt(5))))) +
                (3*(Integer.parseInt(String.valueOf(id.charAt(6))))) +
                (1*(Integer.parseInt(String.valueOf(id.charAt(7))))) +
                (9*(Integer.parseInt(String.valueOf(id.charAt(8))))) +
                (7*(Integer.parseInt(String.valueOf(id.charAt(9)))));
        int lastNumber = Integer.parseInt(String.valueOf(id.charAt(10)));
        int rest = sum%10;
        if(lastNumber == rest) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public Optional<String> getDate() {
        return Optional.empty();
    }
}
