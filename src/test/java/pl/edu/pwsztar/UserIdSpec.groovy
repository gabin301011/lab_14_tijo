package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given:
        def id = new UserId(pesel);
        when:
        def isCorrect = id.isCorrectSize();
        then:
        isCorrect == expected;
        where:
        pesel         || expected
        "98103005472" || true
        "91100456789" || true
        "856735553"   || false
        "56"          || false
        ""            || false
    }

    @Unroll
    def "should get correct sex for pesel: #pesel"() {
        given:
        def id = new UserId(pesel);
        when:
        def userSex = id.getSex();
        then:
        userSex.get() == expected;
        where:
        pesel               || expected
        '79864545644'       || UserIdChecker.Sex.WOMAN
        '67883374708'       || UserIdChecker.Sex.WOMAN
        '44564785393'       || UserIdChecker.Sex.MAN
        '89735838939'       || UserIdChecker.Sex.MAN
        '59843597355'       || UserIdChecker.Sex.MAN
        '78343564764'       || UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should get correct pesel"() {
        given:
        def id = new UserId(pesel);
        when:
        def isCorr = id.isCorrect();
        then:
        isCorr == expected;
        where:
        pesel               || expected
        '86051489399'       || true
        '98071805851'       || true
        '81010200141'       || true
        '81030567389'       || false
        '56673937'          || false
    }
}
